#!/usr/bin/python3

# https://gitlab.com/ndiamistrator/socialcalc_backup.git

import sys
import os
import argparse
import subprocess
import time

import http.client
import json
import datetime

import logging
import tempfile
import re
import html
import multiprocessing

try:
    TAG = sys.argv[1]
except IndexError:
    logger.error('EtherCalc tag not specified!')
    sys.exit(1)

home = os.environ['HOME']
DIR = f'{home}/w/{TAG}_ethercalc'
os.chdir(DIR)

logger = logging.getLogger(__name__)

style = '''<style type='text/css'>
  .%(TAG)s_modified, %(TAG)s_deleted, %(TAG)s_added {
    font-weight: bold !important;
    font-size: large !important;
    }
  .%(TAG)s_modified:before { content: "Modified: " !important; }
  .%(TAG)s_deleted:before { content: "Deleted: " !important; }
  .%(TAG)s_added:before { content: "Added: " !important; }
  .highlighter td { background-image: none !important; }
  .highlighter .add { background-color: #7fff7f !important; }
  .highlighter .remove { background-color: #ff7f7f !important; }
  .highlighter td.modify { background-color: #7f7fff !important; }
  .highlighter td.conflict { background-color: #f00 !important; }
  .highlighter .spec { background-color: #aaa !important; }
  .highlighter .move { background-color: #ffa !important; }
  .highlighter .null { color: #888 !important; }
  .highlighter table { border-collapse:collapse !important; }
  .highlighter td, .highlighter th {
    border: 1px solid #2D4068 !important;
    padding: 3px 7px 2px !important;
    }
  .highlighter th, .highlighter .header, .highlighter .meta {
    background-color: #aaf !important;
    font-weight: bold !important;
    padding-bottom: 4px !important;
    padding-top: 5px !important;
    text-align:left !important;
    }
  .highlighter tr.header th { border-bottom: 2px solid black !important; }
  .highlighter tr.index td, .highlighter .index, .highlighter tr.header th.index {
    background-color: white !important;
    border: none !important;
    }
  .highlighter .gap { color: #888 !important; }
  .highlighter td { empty-cells: show !important; }
</style>''' %{'TAG':TAG}

# TODO
try:
    subprocess.check_call('rm -rf ethercalc.org'.split())
except subprocess.CalledProcessError as e:
    logger.error('Error removing ethercalc.org')
    sys.exit(e.returncode)
try:
    subprocess.check_call(f'python3 {home}/bin/ethercalc.py https://ethercalc.org/{TAG}'.split())
except subprocess.CalledProcessError as e:
    if e.output:
        print(e.output.decode(), end='')
    logger.error('Error calling ethercalc.py')
    sys.exit(e.returncode)


date = time.strftime('%F_%T')

if os.path.exists('last'):
    # diff #################################

    deleted = []
    added = []
    modified = {}

    seen = []

    os.chdir(f'last')
    for file in os.listdir():
        seen.append(file)
        if not file.endswith('.csv.json') or file == f'{TAG}_updates.csv.json': continue
        page = file[:-9]
        if not os.path.exists(f'../ethercalc.org/{file}'):
            deleted.append(page)
            continue
        try:
            daff = subprocess.check_output(['daff', file, f'../ethercalc.org/{file}', '--output-format', 'html', '--fragment', '--fail-if-diff', '--ordered', '--context', '0'],
                stderr=sys.stderr)
        except subprocess.CalledProcessError as e:
            if e.returncode == 1:
                modified[page] = e.output.decode(errors='replace')
            else:
                sys.exit(e.returncode)
        # if len(daff.split(b'\n')) != 6:
        #     modified[page] = daff.decode(errors='replace')
        #     # <table>
        #     # <thead>
        #     # <tr class="header">...</tr>
        #     # </thead>
        #     # </table>
    os.chdir(f'../ethercalc.org')
    for file in os.listdir():
        if file in seen or not file.endswith('.csv.json') or file == f'{TAG}_updates.csv.json': continue
        added.append(file[:-9])
    os.chdir(f'..')

    diff = []
    for page in deleted:
        diff.append(f'<div class="{TAG}_deleted">%s</div>' %html.escape(page))
    for page in added:
        diff.append(f'<div class="{TAG}_added">%s</div>' %html.escape(page))
    for page, daff in modified.items():
        diff.append(f'<div class="{TAG}_modified">%s</div>' %html.escape(page))
        diff.append(f'<div class="highlighter">\n{daff}\n</div>')
    diff = '\n'.join(diff)

    # tmp = tempfile.mkdtemp()
    # print(f'TMP: {tmp}', file=sys.stderr)
    # os.mkdir(f'{tmp}/old')
    # os.mkdir(f'{tmp}/new')
    # for which, dir in (('old', 'last/_'), ('new', 'ethercalc.org/_')):
    #     os.chdir(f'{DIR}/{dir}')
    #     for page in os.listdir():
    #         if page == f'{TAG}_updates': continue
    #         lines = open(page).read().split('\n')
    #         for line in lines:
    #             if line.startswith('sheet:'):
    #                 rows = int(line.split(':')[4])
    #                 break
    #         sub = re.compile('^((?:cell|row|col):)([A-Z]*)([0-9]+)').sub
    #         def proc(m):
    #             # return ''.join([m.group(1), str(rows+1-int(m.group(2)))])
    #             return ''.join([m.group(1), 'X'])
    #         _lines = []
    #         for line in lines:
    #             _lines.append(sub(proc, line))
    #         p = page.lstrip(dir)
    #         with open(f'{tmp}/{which}/{page}', 'w') as f:
    #             f.write('\n'.join(_lines))

    # os.chdir(tmp)

    # try:
    #     diff = subprocess.check_output('diff -r old new'.split(),
    #         stderr=sys.stderr)
    # except subprocess.CalledProcessError as e:
    #     if e.returncode == 1:
    #         diff = e.output
    #     else:
    #         sys.exit(e.returncode)
    # else:
    #     sys.exit()

    if not diff:
        print('OK')
        sys.exit()

    os.chdir(DIR)

    # print()
    # print(diff.decode())
    # with open(f'{date}.diff', 'wb') as f:
    #     f.write(diff)

    
    with open(f'{date}.html', 'w') as f:
        f.write(f'<html><head>\n{style}\n</head><body>\n{diff}\n</body></html>\n')

    multiprocessing.Process(target=subprocess.call,
            args=[['x-www-browser', f'{date}.html']],
            daemon=True).start()

    # TODO
    os.rename('ethercalc.org', date)
    os.unlink('last')
    try: os.unlink('last.html')
    except Exception: pass
    os.symlink(date, 'last')
    os.symlink(f'{date}.html', 'last.html')
    # print(diff)
    # sys.exit()


    # update ###############################

    now = datetime.datetime.now()
    today = datetime.datetime(now.year, now.month, now.day)
    ddate = (today - datetime.datetime(1899,12,30)).days
    dtime = (now.hour*60*60 + now.minute*60 + now.second) / (24*60*60)
    ddatetime = ddate + dtime
    datetimeSTR = now.strftime('%Y-%m-%d %H:%M')

    _diff = diff.replace('\\n', '\\\x00n').replace('\n', '\\n')
    body = json.dumps({'command': [
        'insertrow A3',
        f'set A3 constant ndt {ddatetime} {_diff}',
        f'set B3 text th {_diff}',
    ]})

    try:
        conn = http.client.HTTPSConnection('ethercalc.org')
        conn.request('POST', f'/_/{TAG}_updates', body=body, headers={'Content-Type':'application/json'})
    except Exception as e:
        logger.error(f'Cannot connect to https://ethercalc.org: {e}')
        sys.exit(1)

    try:
        response = conn.getresponse()
        code = response.getcode()
    except Exception as e:
        logger.error(f'Cannot get response from https://ethercalc.org: {e}')
        sys.exit(1)

    if code == 202:
        print('OK')
    else:
        _response = response.read()
        try:
            _response = _response.decode()
        except Exception:
            pass
        else:
            _response = '\n    '.join(_response.split('\n'))
        logger.error(f'{_response}\n\nHTTPS {code}')
        sys.exit(1)

else:
    os.rename('ethercalc.org', date)
    os.symlink(date, 'last')
