#!/usr/bin/python3

# https://gitlab.com/ndiamistrator/socialcalc_backup.git

import sys
import os
import aiohttp
import asyncio
import urllib
import traceback
import re
from bs4 import BeautifulSoup
import logging


FROM = 'lite.framacalc.org'
TO   = 'ethercalc.org'


logger = logging.getLogger(__name__)
info = logger.info
error = logger.error
exception = logger.exception


def log(*a):
    print(*a, file=sys.stderr)

sema = asyncio.Semaphore(5)

sub = re.compile(re.escape(FROM).encode(), re.I).sub
_TO = TO.encode()
def rename(x):
    return sub(_TO, x)

def splitURL(url):
    purl = urllib.parse.urlparse(url)
    part = [x for x in purl.path.lstrip('/').split('/') if x]
    try:
        tail = part[-1]
    except IndexError:
        tail = ''
    head = ('/'.join([purl.netloc]+part[:-1])).lower()
    return head, tail

async def move(page):
 try:
  async with sema:
    log(f'moving: {page}')

    async with session.get(f'https://{FROM}/_/{page}') as response:
        data = await response.read()
    log(f'got: {page}')

    data = rename(data)

    async with session.put(f'https://{TO}/_/{page}',
            data=data, headers={'Content-Type': 'text/x-socialcalc'}) as response:
        r = await response.read()
        assert r == b'OK', AssertionError(r)
    log(f'moved: {page}')

    tasks = []

    async with session.get(f'https://{FROM}/{page}.html') as response:
        data = await response.read()

    _FROM = FROM.lower()
    soup = BeautifulSoup(data, features='lxml')
    for anchor in soup.find_all('a'):
        try:
            link = anchor['href']
        except KeyError:
            continue
        head, tail = splitURL(link)
        if not head or head == _FROM:
            tasks.append(asyncio.ensure_future(move(tail)))
    
  await asyncio.gather(*tasks)
  print(f'done: {page}')

 except Exception:
     print(f'error: {page}')
     traceback.print_exc()

    
async def main(home):
    global session

    async with aiohttp.ClientSession() as session:
        await move(home)


if __name__ == '__main__':
    info = print

    # logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())

    try:
        name = sys.argv[1]
    except IndexError:
        logger.error('name not specified!')
        sys.exit(1)
    
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(name))
    print('DONE')
