#!/usr/bin/python3

# https://gitlab.com/ndiamistrator/socialcalc_backup.git

import os
import sys
import re
import urllib
import asyncio
import json
import aiohttp
import pathlib
import logging
from bs4 import BeautifulSoup


logger = logging.getLogger(__name__)
info = logger.info
error = logger.error
exception = logger.exception


def splitURL(url):
    purl = urllib.parse.urlparse(url)
    part = [x for x in purl.path.lstrip('/').split('/') if x]
    try:
        tail = part[-1]
    except IndexError:
        tail = ''
    head = ('/'.join([purl.netloc]+part[:-1])).lower()
    return head, tail


# def escape(x):
#     x = x.replace('\\', '\\b')
#     x = x.replace('\n', '\\n')
#     x = x.replace('\r', '')
#     x = x.replace(':', '\\c')
#     x = x.replace(']]', '\\e')


class SocialCalcBackup:
    def __init__(S, url, concurrent=8):
        S.prefix, S.home = splitURL(url)
        S.concurrent = concurrent
        S.seen = []
        S.lock = asyncio.Lock()
        S.semaphore = asyncio.Semaphore(S.concurrent)

    async def download(S, url):
        async with S.semaphore:
            async with S.session.get(url) as response:
                return await response.read()

    async def process(S, page):
        tasks = []
        try:
            url = f'https://{S.prefix}/{page}.html'
            data = await S.download(url)

            soup = BeautifulSoup(data, features='lxml')
            for anchor in soup.find_all('a'):
                try:
                    link = anchor['href']
                except KeyError:
                    continue
                head, tail = splitURL(link)
                if not head or head == S.prefix:
                    relative = f'{tail}.html'
                    anchor['href'] = relative
                    tasks.append(asyncio.ensure_future(S.handle(tail)))

            head, tail = splitURL(url)
            pathlib.Path(head).mkdir(parents=True, exist_ok=True)
            with open(f'{head}/{tail}.html', 'wb') as f:
                f.write(str(soup).encode())

        except Exception:
            exception(f'Error processing: {page}')
        
        await asyncio.gather(*tasks)

    async def save(S, url, default=None):
        try:
            head, tail = splitURL(url)
            data = await S.download(url)
            pathlib.Path(head).mkdir(parents=True, exist_ok=True)
            with open(f'{head}/{tail}', 'wb') as f:
                if data:
                    f.write(data)
                elif default is not None:
                    f.write(default)

        except Exception:
            exception(f'Error saving: {url}')
        
    async def handle(S, page):
        async with S.lock:
            if page in S.seen:
                return
            S.seen.append(page)
        S.report(page)
        await asyncio.gather(
            asyncio.ensure_future(S.process(page)),
            asyncio.ensure_future(S.save(f'https://{S.prefix}/_/{page}')),
            asyncio.ensure_future(S.save(f'https://{S.prefix}/{page}.csv.json', b'[""]')),
            *(asyncio.ensure_future(S.save(f'https://{S.prefix}/{page}.{ext}')) for ext in ('ods', 'csv'))
            )

    async def __call__(S, session=None):
        assert not hasattr(S, 'session')
        try:
            if session is None:
                session = aiohttp.ClientSession()
                async with session as _session:
                    S.session = session
                    await S.handle(S.home)
            else:
                S.session = session
                await S.handle(S.home)
        finally:
            del S.session

    def report(S, page):
        info(page)


if __name__ == "__main__":        
    info = print

    # logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())

    try:
        url = sys.argv[1]
    except IndexError:
        logger.error('SocialCalc url not specified!')
        sys.exit(1)

    social = SocialCalcBackup(url)
    loop = asyncio.get_event_loop()
    loop.run_until_complete(social())
    loop.close()
